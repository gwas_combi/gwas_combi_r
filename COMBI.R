COMBI <- function(
  data,
  labels = F,
  genome_wide = F,
  autosome = T,
  feature_embedding = "genotypic",
  center_matrix = T,
  scale_matrix = T,
  feature_scaling_p_norm = 6,
  smoothing_window_size = 35,
  smoothing_filter_p_pnorm = 2,
  svm_classy = "LIBLINEAR_L2R_L1LOSS_SVC_DUAL",
  svm_Cs = 1,
  svm_p = 2,
  svm_epsilon = 1e-7,
  snps_to_keep_absolute = -1,
  snps_to_keep_fraction = 0.05,
  num_perm_vanilla = NULL,
  num_perm = NULL,
  alpha_sig = 0.05,
  save_and_plot = T)
{
  COMBI_method <- function(
    data,
    labels,
    num_snps,
    num_subjects,
    p_scale = feature_scaling_p_norm,
    k = smoothing_window_size,
    p = smoothing_filter_p_pnorm,
    svm_C = svm_Cs,
    type = data_type,
    verbose = TRUE)
  {
    if (verbose == T)
      cat("Creating Matrix for the SVM\n")
    start_time <- Sys.time()
    
    if (type == "rawPlink") {
      num_snp_chr <- ncol(data)/2
      ut8_data <- apply(data, 2, function(x) {sapply(x, function(y) utf8ToInt(y))})
      
      oddvals <- seq(1, ncol(ut8_data), by=2)
      data1 <- ut8_data[,oddvals]
      evenvals <- seq(2, ncol(ut8_data), by=2)
      data2 <- ut8_data[,evenvals]
      
      lexmax_allele_1 <- apply(data1, 2, function(x) max(x))
      lexmax_allele_2 <- apply(data2, 2, function(x) max(x))
      lexmax_overall <- pmax(lexmax_allele_1,lexmax_allele_2)
      data1[data1 == 48] <- 255
      data2[data2 == 48] <- 255
      lexmin_allele_1 <- apply(data1, 2, function(x) min(x))
      lexmin_allele_2 <- apply(data2, 2, function(x) min(x))
      lexmin_overall <- pmin(lexmin_allele_1,lexmin_allele_2)
      
      invalid = which(lexmin_overall == lexmax_overall)
      
      matrixmin <- NULL
      for (i in 1:num_subjects) {
        matrixmin <- rbind(matrixmin, lexmin_overall)
      }
      matrixmax <- NULL
      for (i in 1:num_subjects) {
        matrixmax <- rbind(matrixmax, lexmax_overall)
      }
      
      allele1_lexminor <- t((ut8_data[,oddvals] == matrixmin) + 0)
      allele1_lexmajor <- t((ut8_data[,oddvals] == matrixmax) + 0)
      allele2_lexminor <- t((ut8_data[,evenvals] == matrixmin) + 0)
      allele2_lexmajor <- t((ut8_data[,evenvals] == matrixmax) + 0)
      
      switch (feature_embedding,
          allelic = {
            encoding_factor <- 4
            featmat <- matrix(0, encoding_factor * num_snp_chr, num_subjects)
            featmat[seq(1, nrow(featmat), by=encoding_factor),] <- allele1_lexminor
            featmat[seq(2, nrow(featmat), by=encoding_factor),] <- allele1_lexmajor
            featmat[seq(3, nrow(featmat), by=encoding_factor),] <- allele2_lexminor
            featmat[seq(4, nrow(featmat), by=encoding_factor),] <- allele2_lexmajor
            featmat[(invalid - 1) * encoding_factor + 1, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 2, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 3, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 4, ] <- 0
          },
          genotypic = {
            encoding_factor <- 3
            featmat <- matrix(0, encoding_factor * num_snp_chr, num_subjects)
            featmat[seq(1, nrow(featmat), by=encoding_factor),] <- (allele1_lexminor & allele2_lexminor) + 0
            featmat[seq(2, nrow(featmat), by=encoding_factor),] <- (allele1_lexminor & allele2_lexmajor | allele1_lexmajor & allele2_lexminor) + 0
            featmat[seq(3, nrow(featmat), by=encoding_factor),] <- (allele1_lexmajor & allele2_lexmajor) + 0
            featmat[(invalid - 1) * encoding_factor + 1, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 2, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 3, ] <- 0
          },
          nominal = {
            encoding_factor <- 1
            marginalsMinor <- rowSums(allele1_lexminor) + rowSums(allele2_lexminor)
            marginalsMajor <- rowSums(allele1_lexmajor) + rowSums(allele2_lexmajor)
            marginals <- pmin(marginalsMinor, marginalsMajor)
            risk_allele <- rep(0, nrow(allele1_lexminor))
            risk_allele[which(marginals == marginalsMinor)] <- 1
            risk_allele[which(marginals == marginalsMajor)] <- 2
            risk_allele_is_lexminor <- (risk_allele == 1) + 0
            risk_allele_is_lexmajor <- (risk_allele == 2) + 0
            matrixismin <- NULL
            for (i in 1:num_subjects) {
              matrixismin <- rbind(matrixismin, risk_allele_is_lexminor)
            }
            matrixismin <- t(matrixismin)
            matrixismax <- NULL
            for (i in 1:num_subjects) {
              matrixismax <- rbind(matrixismax, risk_allele_is_lexmajor)
            }
            matrixismax <- t(matrixismax)
            featmat <- (allele1_lexminor + allele2_lexminor) * matrixismin + (allele1_lexmajor + allele2_lexmajor) * matrixismax
            featmat[invalid, ] <- 0
          },{
            stop("type of feature embedding invalid")
          }
      )
    } else if (type=="binaryPlink") {
      num_snp_chr <- ncol(data)
      switch (feature_embedding,
          allelic = {
            encoding_factor <- 4
          },
          genotypic = {
            encoding_factor <- 3
          },
          nominal = {
            encoding_factor <- 1
          },{
            stop("type of feature embedding invalid")
          }
      )
      
      featmat <- NULL
      MatrixCalculus <- function(singlesnp){
        matrixsnp <- matrix(0, num_subjects ,3)
        matrixsnp[which(singlesnp== "01"),1] <- 1
        matrixsnp[which(singlesnp== "02"),2] <- 1
        matrixsnp[which(singlesnp== "03"),3] <- 1
        return(matrixsnp)
      }
      
      matrix_list <- lapply(data, function(x) MatrixCalculus(x))
      featmat <- do.call(cbind, matrix_list)
      rm(matrix_list)
      featmat <- t(featmat)
    } else {
      stop('Unsupported data type: ', type);
    }
    
    end_time <- Sys.time()
    dif_time <- end_time - start_time
    #cat(paste(dif_time, "\n", sep=""))
    
    if (verbose == T)
      cat("Centering Matrix\n")
    start_time <- Sys.time()
    ## CENTER DATA MATRIX
    if (center_matrix == T) {
      featmat <- t(scale(t(featmat), center = TRUE, scale = FALSE))
    }
    
    end_time <- Sys.time()
    dif_time <- end_time - start_time
    #cat(paste(dif_time, "\n", sep=""))
    if (verbose == T)
      cat("Scaling Matrix\n")
    start_time <- Sys.time()
    ## SCALE DATA MATRIX
    if (scale_matrix == T) {
      dims <- dim(featmat)[1]
      n <- dim(featmat)[2]
      d <- dims
      
      stddev <- matrix(apply(featmat,1, function(x) (mean(abs(x)^p_scale)*d)^(1/p_scale)), ncol=1)
      featmat <- featmat / stddev %*% rep(1,n)
      featmat[is.nan(featmat) == TRUE] <- 0
    }
    
    end_time <- Sys.time()
    dif_time <- end_time - start_time
    #cat(paste(dif_time, "\n", sep=""))
    
    if (verbose == T)
      cat("Running SVM\n")
    start_time <- Sys.time()
    ## SVM
    switch (svm_classy,
        LIBLINEAR_L2R_LR = {
          svm_type <- 0
        },
        LIBLINEAR_L2R_L2LOSS_SVC_DUAL = {
          svm_type <- 1
        },
        LIBLINEAR_L2R_L2LOSS_SVC = {
          svm_type <- 2
        },
        LIBLINEAR_L2R_L1LOSS_SVC_DUAL = {
          svm_type <- 3
        },{
          stop("svm classifier not supported")
        }
    )
    
    library(LiblineaR)
    res <- LiblineaR(
      data = t(featmat),
      target = as.character(labels),
      type = svm_type,
      cost = svm_C,
      epsilon = svm_epsilon,
      bias = FALSE)
    
    rm(featmat)
    weights_raw <- res$W
    rm(res)
    
    weights_normalised <- abs(weights_raw) / base::norm(weights_raw, type="2")
    
    weights_per_snp_matrix <- matrix(weights_raw, ncol=(length(weights_raw)/encoding_factor))
    weights_per_snp_normalized_matrix <- matrix(weights_normalised, ncol=(length(weights_raw)/encoding_factor))
    
    if (encoding_factor > 1) {
      w <- colSums(abs(weights_per_snp_matrix)^svm_p)^(1/svm_p)
    } else {
      w <- weights_per_snp_matrix
    }
    
    
    ## Moving Average filter applied to the weights generated by the SVM
    ## Checking if the smoothing_window_size parameter is an odd value
    if (k %% 2 == 0) {
      stop("smoothing_window_size parameter must be an odd number")
    }
    
    if (k == 1) {
      w_smooth <- w
    } else {
      if (p == 1) {
        w_smooth <- c(rep(0,(k-1)/2 + 1), w, rep(0, (k-1)/2))
        w_smooth <- cumsum(w_smooth)
        w_smooth <- w_smooth[-(1:k)] - w_smooth[-((length(w_smooth)-k+1) :length(w_smooth))]
        w_smooth <- w_smooth / k
      } else {
        w_mod <- w^p
        w_smooth <- c(rep(0,(k-1)/2 + 1), w_mod, rep(0, (k-1)/2))
        w_smooth <- cumsum(w_smooth)
        w_smooth <- sapply(w_smooth[-(1:k)] - w_smooth[-((length(w_smooth)-k+1) :length(w_smooth))], function(x) x^(1/p))
        w_smooth <- w_smooth / (k^(1/p))
      }
    }
    
    
    end_time <- Sys.time()
    dif_time <- end_time - start_time
    #cat(paste(dif_time, "\n", sep=""))
    if (verbose == T) {
      cat("SVM ran in:\n")
      dif_time
    }
    
    if (verbose == T) 
      cat("CHI Squared test\n")
    start_time <- Sys.time()
    ## CHI SQUARED TEST
    if (type == "rawPlink") {
      cases <- (labels == 1) + 0
      controls <- (labels == -1) + 0
      valid <- ((data1 != 255) & (data2 != 255)) + 0
      valid_cases <- t((valid & cases) + 0)
      valid_controls <- t((valid & controls) + 0)
      not_equal_mat <- (allele1_lexminor != allele2_lexminor)+0
      
      tables <- replicate(num_snp_chr, matrix(, nrow=2, ncol=3), simplify=F)
      
      values_11 <- rowSums(allele2_lexminor & allele1_lexminor & valid_cases)
      for (i in 1:num_snp_chr) {
        tables[[i]][1,1] <- values_11[i]
      }
      values_12 <- rowSums(not_equal_mat & valid_cases)
      for (i in 1:num_snp_chr) {
        tables[[i]][1,2] <- values_12[i]
      }
      values_13 <- rowSums(valid_cases) - values_11 - values_12
      for (i in 1:num_snp_chr) {
        tables[[i]][1,3] <- values_13[i]
      }
      
      values_21 <- rowSums(allele2_lexminor & allele1_lexminor & valid_controls)
      for (i in 1:num_snp_chr) {
        tables[[i]][2,1] <- values_21[i]
      }
      values_22 <- rowSums(not_equal_mat & valid_controls)
      for (i in 1:num_snp_chr) {
        tables[[i]][2,2] <- values_22[i]
      }
      values_23 <- rowSums(valid_controls) - values_21 - values_22
      for (i in 1:num_snp_chr) {
        tables[[i]][2,3] <- values_23[i]
      }
    } else if (type == "binaryPlink") {
      data2 <- data.frame(data)
      data2 <- apply(data2,2,function(x) as.character(x))
      data2[data2=="00"] <- NA
      data <- data.table(data2)
      rm(data2)
      tables <- replicate(num_snp_chr, matrix(, nrow=2, ncol=3), simplify=F)
      for (i in 1:num_snp_chr) {
        tables[[i]] <- table(labels, data[[i]])
      }
    }
    
    ## Calculate Chi Squared pvalue
    pvalues <- unlist(lapply(tables, function(x) chisq.test(x[,!apply(x==0,2,all)])$p.value))
    ## remove pvalues of tables with just one dimension
    pvalues[which(lapply(tables, function(x) is.null(dim(x[,!apply(x==0,2,all)])))==T)] <-NA
    
    Xsqrd <- unlist(lapply(tables, function(x) chisq.test(x[,!apply(x==0,2,all)])$statistic))
    
    
    if (snps_to_keep_absolute == -1) {
      snps_to_keep_absolute <- ceiling(num_snp_chr * snps_to_keep_fraction)
    }
    
    pvalues[which(!w %in% w[order(-w_smooth)][1:snps_to_keep_absolute])] <- 1
    
    end_time <- Sys.time()
    dif_time <- end_time - start_time
    #cat(paste(dif_time, "\n", sep=""))
    if (verbose == T) {
      cat("P-Values calculation ran in:\n")
      dif_time
    }
    
    return(pvalues)
  }

  chi_square_goodnes_of_fit_test<- function(
    data,
    labels,
    num_snps,
    num_subjects,
    type = data_type)
  {
    start_time <- Sys.time()
    
    if (type == "rawPlink") {
      num_snp_chr <- ncol(data)/2
      ut8_data <- apply(data, 2, function(x) {sapply(x, function(y) utf8ToInt(y))})
      
      oddvals <- seq(1, ncol(ut8_data), by=2)
      data1 <- ut8_data[,oddvals]
      evenvals <- seq(2, ncol(ut8_data), by=2)
      data2 <- ut8_data[,evenvals]
      
      lexmax_allele_1 <- apply(data1, 2, function(x) max(x))
      lexmax_allele_2 <- apply(data2, 2, function(x) max(x))
      lexmax_overall <- pmax(lexmax_allele_1,lexmax_allele_2)
      data1[data1 == 48] <- 255
      data2[data2 == 48] <- 255
      lexmin_allele_1 <- apply(data1, 2, function(x) min(x))
      lexmin_allele_2 <- apply(data2, 2, function(x) min(x))
      lexmin_overall <- pmin(lexmin_allele_1,lexmin_allele_2)
      
      invalid = which(lexmin_overall == lexmax_overall)
      
      matrixmin <- NULL
      for (i in 1:num_subjects) {
        matrixmin <- rbind(matrixmin, lexmin_overall)
      }
      matrixmax <- NULL
      for (i in 1:num_subjects) {
        matrixmax <- rbind(matrixmax, lexmax_overall)
      }
      
      allele1_lexminor <- t((ut8_data[,oddvals] == matrixmin) + 0)
      allele1_lexmajor <- t((ut8_data[,oddvals] == matrixmax) + 0)
      allele2_lexminor <- t((ut8_data[,evenvals] == matrixmin) + 0)
      allele2_lexmajor <- t((ut8_data[,evenvals] == matrixmax) + 0)
      
      switch (feature_embedding,
          allelic = {
            encoding_factor <- 4
            featmat <- matrix(0, encoding_factor * num_snp_chr, num_subjects)
            featmat[seq(1, nrow(featmat), by=encoding_factor),] <- allele1_lexminor
            featmat[seq(2, nrow(featmat), by=encoding_factor),] <- allele1_lexmajor
            featmat[seq(3, nrow(featmat), by=encoding_factor),] <- allele2_lexminor
            featmat[seq(4, nrow(featmat), by=encoding_factor),] <- allele2_lexmajor
            featmat[(invalid - 1) * encoding_factor + 1, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 2, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 3, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 4, ] <- 0
          },
          genotypic = {
            encoding_factor <- 3
            featmat <- matrix(0, encoding_factor * num_snp_chr, num_subjects)
            featmat[seq(1, nrow(featmat), by=encoding_factor),] <- (allele1_lexminor & allele2_lexminor) + 0
            featmat[seq(2, nrow(featmat), by=encoding_factor),] <- (allele1_lexminor & allele2_lexmajor | allele1_lexmajor & allele2_lexminor) + 0
            featmat[seq(3, nrow(featmat), by=encoding_factor),] <- (allele1_lexmajor & allele2_lexmajor) + 0
            featmat[(invalid - 1) * encoding_factor + 1, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 2, ] <- 0
            featmat[(invalid - 1) * encoding_factor + 3, ] <- 0
          },
          nominal = {
            encoding_factor <- 1
            marginalsMinor <- rowSums(allele1_lexminor) + rowSums(allele2_lexminor)
            marginalsMajor <- rowSums(allele1_lexmajor) + rowSums(allele2_lexmajor)
            marginals <- pmin(marginalsMinor, marginalsMajor)
            risk_allele <- rep(0, nrow(allele1_lexminor))
            risk_allele[which(marginals == marginalsMinor)] <- 1
            risk_allele[which(marginals == marginalsMajor)] <- 2
            risk_allele_is_lexminor <- (risk_allele == 1) + 0
            risk_allele_is_lexmajor <- (risk_allele == 2) + 0
            matrixismin <- NULL
            for (i in 1:num_subjects) {
              matrixismin <- rbind(matrixismin, risk_allele_is_lexminor)
            }
            matrixismin <- t(matrixismin)
            matrixismax <- NULL
            for (i in 1:num_subjects) {
              matrixismax <- rbind(matrixismax, risk_allele_is_lexmajor)
            }
            matrixismax <- t(matrixismax)
            featmat <- (allele1_lexminor + allele2_lexminor) * matrixismin + (allele1_lexmajor + allele2_lexmajor) * matrixismax
            featmat[invalid, ] <- 0
          },{
            stop("type of feature embedding invalid")
          }
      )
    } else if (type == "binaryPlink") {
      num_snp_chr <- ncol(data)
      switch (feature_embedding,
          allelic = {
            encoding_factor <- 4
          },
          genotypic = {
            encoding_factor <- 3
          },
          nominal = {
            encoding_factor <- 1
          },{
            stop("type of feature embedding invalid")
          }
      )
      
      featmat <- NULL
      MatrixCalculus <- function(singlesnp){
        matrixsnp <- matrix(0, num_subjects ,3)
        matrixsnp[which(singlesnp == "01"),1] <- 1
        matrixsnp[which(singlesnp == "02"),2] <- 1
        matrixsnp[which(singlesnp == "03"),3] <- 1
        return(matrixsnp)
      }
      
      matrix_list <- lapply(data, function(x) MatrixCalculus(x))
      featmat <- do.call(cbind, matrix_list)
      rm(matrix_list)
      featmat <- t(featmat)
    } else {
      stop('Unsupported data type: ', type);
    }
  
    start_time <- Sys.time()
    ## CHI SQUARED TEST
    if (type == "rawPlink") {
      cases <- (labels == 1) + 0
      controls <- (labels == -1) + 0
      valid <- ((data1 != 255) & (data2 != 255)) + 0
      valid_cases <- t((valid & cases) + 0)
      valid_controls <- t((valid & controls) + 0)
      not_equal_mat <- (allele1_lexminor != allele2_lexminor)+0
      
      tables <- replicate(num_snp_chr, matrix(, nrow=2, ncol=3), simplify=F)
      
      values_11 <- rowSums(allele2_lexminor & allele1_lexminor & valid_cases)
      for (i in 1:num_snp_chr) {
        tables[[i]][1,1] <- values_11[i]
      }
      values_12 <- rowSums(not_equal_mat & valid_cases)
      for (i in 1:num_snp_chr) {
        tables[[i]][1,2] <- values_12[i]
      }
      values_13 <- rowSums(valid_cases) - values_11 - values_12
      for (i in 1:num_snp_chr) {
        tables[[i]][1,3] <- values_13[i]
      }
      
      values_21 <- rowSums(allele2_lexminor & allele1_lexminor & valid_controls)
      for (i in 1:num_snp_chr) {
        tables[[i]][2,1] <- values_21[i]
      }
      values_22 <- rowSums(not_equal_mat & valid_controls)
      for (i in 1:num_snp_chr) {
        tables[[i]][2,2] <- values_22[i]
      }
      values_23 <- rowSums(valid_controls) - values_21 - values_22
      for (i in 1:num_snp_chr) {
        tables[[i]][2,3] <- values_23[i]
      }
    } else if (type=="binaryPlink") {
      data2 <- data.frame(data)
      data2 <- apply(data2,2,function(x) as.character(x))
      data2[data2=="00"] <- NA
      data <- data.table(data2)
      rm(data2)
      tables <- replicate(num_snp_chr, matrix(, nrow=2, ncol=3), simplify=F)
      for (i in 1:num_snp_chr) {
        tables[[i]] <- table(labels, data[[i]])
      }
    }
    
    ## Calculate Chi Squared pvalue
    pvalues <- unlist(lapply(tables, function(x) chisq.test(x[,!apply(x==0,2,all)])$p.value))
    ## remove pvalues of tables with just one dimension
    pvalues[which(lapply(tables, function(x) is.null(dim(x[,!apply(x==0,2,all)])))==T)] <-NA
    
    Xsqrd <- unlist(lapply(tables, function(x) chisq.test(x[,!apply(x==0,2,all)])$statistic))
   
    end_time <- Sys.time()
    dif_time <- end_time - start_time
    #cat(paste(dif_time, "\n", sep=""))
    return(pvalues)
  }

  permutation_chr_vanilla <- function() {
        lowest_pval <- NULL
        cat("Starting vanilla permutations\n")
        cat("Permutation (vanilla): ")
        perm_start <- proc.time()
        sapply(1:num_perm_vanilla, function(x) {
          results_table_perm <- data.frame(SNP=unlist(lapply(chromosomes, function(x) SNP_list[[x]][,1,with=F]), use.names=F))
          cat(paste(x, "/", num_perm_vanilla, " ...\n", sep=""))
          labels_perm <- sample(labels)
          if (data@type == "rawPlink") {
            num_snps <- length(chromosome_list[[x]]) / 2
          } else if (data@type == "binaryPlink") {
            num_snps <- length(chromosome_list[[x]])
          }
          results <- lapply(chromosomes, function(x) {
            chi_square_goodnes_of_fit_test(data@genomic_data[,chromosome_list[[x]],with=F], labels_perm, num_snps, data@num_subjects)
            })
          names(results) <- chromosomes
          
          results_table_perm_tmp <- lapply(chromosomes, function(x) cbind(SNP_list[[x]], results[[x]]))
          results_table_perm_tmp <- rbindlist(results_table_perm_tmp)
          
          setnames(results_table_perm_tmp, c("SNP","CHR","BP","P"))
          lowest_pval <<- c(lowest_pval, min(results_table_perm_tmp$P))
        })
        lowest_pval <- sort(as.numeric(as.character(lowest_pval)))
        ind <- which(lowest_pval==min(abs(lowest_pval-alpha_sig), na.rm=T))
        alpha_j <- ind/num_perm_vanilla
        perm_time <- proc.time() - perm_start
        if (verbose == T) {
          cat("Permutation (vanilla) ran in:\n")
          perm_time
        }
        return(alpha_j)
}

  permutation_chr <- function(){
        lowest_pval <- NULL
        cat("Starting permutations\n")
        cat("Permutation: ")
        perm_start <- proc.time()
        sapply(1:num_perm, function(x){
          results_table_perm <- data.frame(SNP=unlist(lapply(chromosomes, function(x) SNP_list[[x]][,1,with=F]), use.names=F))
          cat(paste(x, "/", num_perm, " ...", sep=""))
          labels_perm <- sample(labels)
          if (data@type == "rawPlink") {
            num_snps <- length(chromosome_list[[x]]) / 2
          } else if (data@type == "binaryPlink") {
            num_snps <- length(chromosome_list[[x]])
          }
          results <- lapply(chromosomes, function(x){COMBI_method(data@genomic_data[,chromosome_list[[x]],with=F], labels_perm, num_snps, data@num_subjects, verbose=F)})
          names(results) <- chromosomes
          
          results_table_perm_tmp <- lapply(chromosomes, function(x) cbind(SNP_list[[x]], results[[x]]))
          results_table_perm_tmp <- rbindlist(results_table_perm_tmp)
          
          setnames(results_table_perm_tmp, c("SNP","CHR","BP","P"))
          lowest_pval <<- c(lowest_pval, min(results_table_perm_tmp$P))
        })
        lowest_pval <- sort(as.numeric(as.character(lowest_pval)))
        t_star <- lowest_pval[max(c(ceiling(num_perm*alpha_sig)-1,1), na.rm=T)]
        perm_time <- proc.time() - perm_start
        if (verbose == T) {
          cat("Permutation ran in:\n")
          perm_time
        }
        return(t_star)
  }
  plot_data <- function() {
    if (!is.null(num_perm)) {
      ## split result table given the significance threshold calculated using the permutations
      ## significant SNPs
      results_table_signif <- results_table[P <= t_star]
      write.table(results_table_signif, paste("COMBI_Results_signif_", result_time, ".txt", sep=""), quote=F, sep="\t", row.names=F)
      cat(paste("COMBI results were saved in: ", paste(getwd(),"/COMBI_Results_signif_", result_time, sep=""), "\n", sep=""))
      ## not significant SNPs
      results_table_no_signif <- results_table[P > t_star]
      write.table(results_table_no_signif, paste("COMBI_Results_no_signif_", result_time, ".txt", sep=""), quote=F, sep="\t", row.names=F)
      cat(paste("COMBI results were saved in: ", paste(getwd(),"/COMBI_Results_no_signif_",  result_time, sep=""), "\n", sep=""))
    } else {
      write.table(results_table, paste("COMBI_Results_", result_time, ".txt", sep=""), quote=F, sep="\t", row.names=F)
      cat(paste("COMBI results were saved in: ", paste(getwd(),"/COMBI_Results_",  result_time, sep=""), "\n", sep=""))
    }

    require("qqman")
    results_table$CHR <- as.numeric(results_table$CHR)
    results_table$BP <- as.numeric(results_table$BP)
    results_table$P <- as.numeric(results_table$P)

    jpeg(paste("COMBI_Manhattan_", result_time , ".jpg", sep=""))
    if (!is.null(num_perm)) {
      genomewideline <- -log10(t_star)
    } else {
      genomewideline <- F
    }
    manhattan(results_table[!is.na(results_table$P),], suggestiveline = F, genomewideline = genomewideline)
    dev.off()
    cat(paste("COMBI Manhattan plot was saved in: ", paste(getwd(),"/COMBI_Manhattan_",  result_time, sep=""), "\n", sep=""))
  }


  #######################################
  ####  PIPELINE
  #######################################

  total_time_start <- Sys.time()
  if (class(data)!="COMBI_DATA") {
    stop("Input is not of COMBI_DATA class")
  }
  
  data_type <- data@type
  ## if no manual labels were inserted it will look for the labels in the 6th column of the genomic_data
  if (labels == F) {
    labels <- data@genomic_data[[6]]
  }
  labels <- as.integer(labels)
  labels[labels == 1] <- -1
  labels[labels == 2] <- 1
  labels[labels == -9] <- NA


  if (genome_wide) {
    results <- COMBI_method(data@genomic_data[,7:ncol(data@genomic_data), with=F], labels, data@num_snps, data@num_subjects)
    results_table <- cbind(data@snp_data$V2, data@snp_data$V1, data@snp_data$V4, results)
    if (autosome) {
       keep <- as.character(1:22)
       results_table <- results_table[results_table[,2] %in% keep,]
    }
    results_table <- data.table(results_table)
    setnames(results_table, c("SNP","CHR","BP","P"))

    if (save_and_plot==T) {
      result_time <- format(Sys.time(), "%Y%m%d_%H%M%S")
      plot_data()
    }
    return(results_table)
  } else {
    chromosomes <- levels(as.factor(unlist(unique(data@snp_data[,1,with=F]))))
    chromosomes <- mixedsort(chromosomes)
    if (autosome) {
      keep <- as.character(1:22)
      chromosomes <- chromosomes[chromosomes %in% keep]
    }
    if (data@type == "rawPlink") {
      # TODO Modularize these two lines, to have less copy&pasted code
      chromosome_list <- lapply(chromosomes, function(x) unlist(sapply(which(data@snp_data[,1,with=F]==x), function(y) c((y*2-1)+6,(y*2)+6), simplify=F)))
    } else if (data@type == "binaryPlink") {
      chromosome_list <- lapply(chromosomes, function(x) unlist(sapply(which(data@snp_data[,1,with=F]==x), function(y) y+6, simplify=F)))
    }
    first_column_snp_data <- colnames(data@snp_data)[1]
    SNP_list <- lapply(chromosomes,function(x) data@snp_data[get(first_column_snp_data)==x][,c(2,1,4),with=F])
    names(chromosome_list) <- chromosomes
    names(SNP_list) <- chromosomes

    if (!is.null(num_perm)) {
      if (num_perm<10){
        stop("The number of permutations should be higher than 10")
      } else {
        alpha_j <- permutation_chr_vanilla()
        cat("\n")
        cat(alpha_j)
        t_star <- permutation_chr()
        cat("\n")
        cat(paste("t-star value: ",t_star,"\n",sep=""))
      }
    }

    if (data@type == "rawPlink") {
      num_snps <- length(chromosome_list[[x]]) / 2
    } else if (data@type == "binaryPlink") {
      num_snps <- length(chromosome_list[[x]])
    }
    results <- lapply(chromosomes, function(x){cat(paste("Running the COMBI method for chromosome ", x, " with ", num_snps, " SNPs\n", sep="")); COMBI_method(data@genomic_data[,chromosome_list[[x]],with=F], labels, num_snps, data@num_subjects)})
    names(results) <- chromosomes
    
    results_table <- lapply(chromosomes, function(x) cbind(SNP_list[[x]], results[[x]]))
    results_table <- rbindlist(results_table)
    
    setnames(results_table, c("SNP","CHR","BP","P"))
    
    
    #cat(results_table_perm)
    
    total_time_end <- Sys.time()
    cat(paste("Total elapsed time: ", total_time_end - total_time_start, "\n", sep=""))
    if (save_and_plot==T) {
      result_time <- format(Sys.time(), "%Y%m%d_%H%M%S")
      plot_data()
    }
    return(results_table)
  }
}
